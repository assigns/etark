import "./App.css";
import Navbar from "./components/Navbar";
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import About from "./components/About";

const theme = createTheme({
  typography: {
    fontFamily: ['"Poppins"', "sans-serif"].join(","),
  },
});
function App() {
  return (
    <>
      <Navbar></Navbar>
      <About></About>
    </>
  );
}

export default App;
