import { Accordion, Box, makeStyles } from "@material-ui/core";
import React from "react";
import AccordianCard from "./AccordianCard";
import { technicalData, nonTechnicalData } from "./accordianData";

const useStyles = makeStyles({
  wrapper: {
    textAlign: "center",
    margin: "5vh 5% 0px",
  },
  root: {
    textAlign: "center",
    margin: "3% 20vw ",
    "& p": {
      margin: "2vh 0 0 0",
      textAlign: "left",
    },
    "& h3": {
      margin: "0",
    },
    "& h1": {
      margin: "0",
    },
  },
});

function About() {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <div></div>
      <div className={classes.root}>
        <h1>About Us</h1>
        <div>
          <p>
            ETark is an automated complaint resolution platform for smartphone
            complaints. We help in resolving both technical and non-technical
            smartphone problems via. our service offerings. ETark was
            conceptualized after identifying the plight of hapless smartphone
            customers and the inefficiency of the service centers.
            <br />
            <br />
            How ETark helps customers?
            <br />
            <br />
          </p>
          <h3>Resolving technical problems :</h3>
          <p>
            Having a technical problem with your smartphone can be disastrous
            because unlike any other gadget, a smartphone lets you stay
            connected with the world and manage all your electronic records. The
            current approaches that a user usually takes :
          </p>
          <p>
            {technicalData.map((data) => {
              return (
                <div>
                  <AccordianCard
                    title={data.title}
                    details={data.details}
                  ></AccordianCard>
                  <br />
                </div>
              );
            })}
            <h3 style={{textAlign : "center"}}>Resolving non-technical problems :</h3>
            <p>
              Problems like missing, damaged or duplicate items during purchase
              or Payment related issues like improper bill, wrongful deductions
              etc are some of the examples of non- technical problems that
              customers face.
              <br />
              <br />
              The ideal way to solve this is to reach out to the seller
              (e-commerce firm/ offline retailer) or the device manufacturer
              (e.g. Samsung, Apple etc.). However a customer’s voice can go
              unheard in a discussion with the other party. So to lend a
              strength to the customer’s voice, we do an instant analysis of
              his/ her complaint and share certain reports with him/ her which
              can help identify the merit of the complaint/ grievance.
              <br />
              <br />
             
             {nonTechnicalData.map((data,i)=>{
               return  (
                <div>
                <AccordianCard
                  title={data.title}
                  details={data.details}
                ></AccordianCard>
                {!i ? ( <br></br>) : null} 
              </div>
               )
             })}
            </p>
          </p>
        </div>
      </div>
    </div>
  );
}

export default About;
