export const  technicalData = [
  {
    title: "Visit an authorized service center",
    details:<>
      <p><b>While an authorized service center is always recommended, but as a user, you don’t know if:</b></p><p><ul><p>A. Parts are available at the service center to service your device</p><p>B. Your device would be considered within warranty by the service center</p><p>C. The exact problem in your device and the price to be paid to fix that</p></ul></p><p>All this demands you to visit the service center and wait in a queue with other customers which can often take hours with the possibility of parts not available or the device not considered within warranty by the service center or the quoted price of servicing out of your budget!</p>
      </>
  },
  {
    title: "Visit an unauthorized service center",
    details: <>
    <p><b>Although an unauthorized service center might seem light on your pocket, this route is never recommended because :</b></p><p><ul><li>Due to the poor quality of servicing, they often result in frequent annual repairs for the same issue or some other problem thus making the actual cost of repair for that phone way higher than the cost of repair by an authorized service center</li><li>Your personal data could be retrieved from your device even if you have removed the same from your device. So due to low accountability of unauthorized service centers, data could be stolen from your device during servicing.</li></ul></p>
    </>
  },
  {
    title: "Book for a home visit repair service",
    details: <>
    <p><b>Home visit repair services are quite popular nowadays due to the convenience of home repair that they promise but this should be avoided due to the following reasons:</b></p><p><ul><li>Most of the home repair services don’t have any authorization from the device manufacture to provide servicing, thus the accountability and quality of servicing is low</li><li>Many a times your device demands a part replacement which may not be available for the home visit repair agent at that point and post their inspection of the device at your location, they might take your device to their service center thus causing an additional delay in the servicing of the device</li></ul></p>
    </>
  },
  {
    title: "Device Insurance companies",
    details: <> 
    <p><b>Although device Insurance companies promise to insure your device from future damage or malfunctioning:</b></p><p>They often result in long waiting time to get clearance from the smartphone manufacturer to get the ensuing servicing covered which offsets the cost savings that they promise through their insurance<br/><br/>All these approaches have their own share of flaws.<br/><br/> ETark firmly believes that the best way to solve any technical issue is through authorized service centers only provided the inefficiencies of the service centers are removed and the customer doesn’t have to go through any hassle. This is exactly where we step in and we ensure that both the customer and the service center are seamlessly connected!</p>
    </>
  },
];


export const nonTechnicalData =[
  {
    title: "Chances of winning",
    details: <>
    <p>Chances of winning tells you how likely you are to win in a consumer court had the complaint been filed there<br/><br/>Or Alternatively,<br/><br/>How likely you are to win in a negotiation with the other party (device manufacturer or seller) by quoting your winning chances in a consumer court as a reference</p>
    </>
  },
  {
    title: "Expected compensation",
    details: <>
    Expected compensation tells you what compensation (free servicing, product/ part replacement or monetary compensation) you are likely to get from the other party had your complaint been filed in a consumer court<br/><br/>Or Alternatively,<br/><br/>what compensation (free servicing, product/ part replacement) you are likely to get from the other party in a negotiation
    </>
  },
]